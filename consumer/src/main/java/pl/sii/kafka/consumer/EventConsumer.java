package pl.sii.kafka.consumer;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Component;
import pl.sii.kafka.consumer.model.EventRequestModel;


@Component
@Slf4j
@RequiredArgsConstructor
public class EventConsumer {

    @KafkaListener(
            topics = "${topic.name.event-request}",
            containerFactory = "eventKafkaListenerContainerFactory"
    )
    public void readCommand(EventRequestModel EventModel) {
        log.info("Event received: {}", EventModel); }


//    @KafkaListener(topicPartitions = @TopicPartition(topic = "${topic.name.event-request}", partitions = {"0"})
//            , containerFactory = "eventKafkaListenerContainerFactory")
//    public void readCommandPartition1(EventRequestModel EventModel) {
//        log.info("Event received: {}", EventModel); }
}
