package pl.sii.kafka.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.*;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import pl.sii.kafka.producer.model.EventRequestModel;


import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Strings.isNullOrEmpty;

@Configuration
@EnableKafka
@Slf4j
public class KafkaConfig {

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Value("${spring.application.name}")
    private String appName;

    @Value("${app.kafka.group.id.suffix}")
    private String groupIdSuffix;

    @Bean
    public KafkaTemplate<String, EventRequestModel> eventKafkaTemplate() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.registerModule(new JavaTimeModule());

        ProducerFactory<String, EventRequestModel> producerFactory = new DefaultKafkaProducerFactory<>(props, new StringSerializer(), new JsonSerializer<>(objectMapper));

        KafkaTemplate<String, EventRequestModel> kt = new KafkaTemplate<>(producerFactory);
        kt.setProducerListener(new ProducerListener<String, EventRequestModel>() {

            @Override
            public void onSuccess(ProducerRecord<String, EventRequestModel> record, RecordMetadata recordMetadata) {
                log.info("### Callback :: {} ; partition = {} with offset= {} ; Timestamp : {} ; Message Size = {}", recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset(), recordMetadata.timestamp() , recordMetadata.serializedValueSize());
            }

            @Override
            public void onError(ProducerRecord<String, EventRequestModel> producerRecord, Exception exception) {
                log.error("### Topic = {}; Message = {}; Error = {}",producerRecord.topic(), producerRecord.value(), exception);
            }
        });

        return kt;
    }

    private Map<String, Object> getCommonProps() {
        String groupId = appName;
        if (!isNullOrEmpty(groupIdSuffix)) {
            groupId = appName + "-" + groupIdSuffix;
        }

        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put("spring.json.use.type.headers", "false");

        return props;
    }
}
