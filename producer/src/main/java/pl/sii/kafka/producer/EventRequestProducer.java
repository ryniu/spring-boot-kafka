package pl.sii.kafka.producer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.util.concurrent.SuccessCallback;
import pl.sii.kafka.producer.model.EventRequestModel;

@Component
@Slf4j
public class EventRequestProducer {

    @Autowired
    private KafkaTemplate<String, EventRequestModel> eventKafkaTemplate;

    @Value("${topic.name.event-request}")
    private String topic;

    public void sendRequestToMongo(EventRequestModel eventRequest) {
        eventKafkaTemplate.send(topic, eventRequest);
    }
}
