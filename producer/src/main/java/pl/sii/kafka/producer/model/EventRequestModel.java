package pl.sii.kafka.producer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class EventRequestModel {
    @JsonProperty("id")
    private long id;
}
