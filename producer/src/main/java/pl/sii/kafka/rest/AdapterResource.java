package pl.sii.kafka.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.sii.kafka.producer.EventRequestProducer;
import pl.sii.kafka.producer.model.EventRequestModel;


@RestController
@RequestMapping("/api/adapter")
public class AdapterResource {
    private final Logger log = LoggerFactory.getLogger(AdapterResource.class);

    @Autowired
    private EventRequestProducer eventRequestProducer;


    @GetMapping
    public ResponseEntity<String> getEvent(@RequestParam long id) {
        eventRequestProducer.sendRequestToMongo(EventRequestModel.builder().id(id).build());
        return ResponseEntity.ok().header("Access-Control-Allow-Origin", "*").body("ok");
    }
}